﻿namespace AdventOfCode
{
    public interface IDaySolver
    {
        public string Name { get; }
        void Solve(string[] input);
    }
}
