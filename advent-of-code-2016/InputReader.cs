﻿using System;
using System.IO;

namespace AdventOfCode
{
    public static class InputReader
    {
        private const string INPUT_PATH = @"..\..\..\Inputs\";
        private const string FILE_EXTENSION = "txt";

        public static string[] GetInput(string fileName)
        {
            string path = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, INPUT_PATH, $"{fileName}.{FILE_EXTENSION}"));

            string[] result = File.ReadAllLines(path);

            return result;
        }
    }
}
