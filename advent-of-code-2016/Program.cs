﻿using System;
using System.Collections.Generic;

namespace AdventOfCode
{
    internal static class Program
    {
        private const int NUMBER_OF_DAYS = 25;
        private const string SOLVER_NAMESPACE = "AdventOfCode";
        private const string SOLVER_PREFIX = "Day";
        private const string SOLVER_SUFFIX = "Solver";

        private static readonly List<IDaySolver> _solversList = new List<IDaySolver>();

        private static void Main(string[] args)
        {
            Console.WriteLine("Advent of code 2016!\n");

            GetSolvers();
            ExecuteSolvers();
        }

        private static void GetSolvers()
        {
            for (int i = 0; i < NUMBER_OF_DAYS; i++)
            {
                string solverName = $"{SOLVER_NAMESPACE}.{SOLVER_PREFIX}{i + 1}{SOLVER_SUFFIX}";
                Type? type = Type.GetType(solverName);

                if (type == null)
                {
                    continue;
                }

                object? obj = Activator.CreateInstance(type);

                if (obj == null)
                {
                    continue;
                }

                _solversList.Add((IDaySolver) obj);
            }
        }

        private static void ExecuteSolvers()
        {
            foreach (IDaySolver daySolver in _solversList)
            {
                string[] inputLines = InputReader.GetInput(daySolver.Name);

                daySolver.Solve(inputLines);
            }
        }
    }
}
