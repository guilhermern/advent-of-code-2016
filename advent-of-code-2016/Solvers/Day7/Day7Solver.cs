﻿using System;

namespace AdventOfCode.Day7
{
    public sealed class Day7Solver : IDaySolver
    {
        private const string NAME = "Day7";

        public string Name => NAME;

        public void Solve(string[] input)
        {
            Console.WriteLine("Day 7");

            int tlsCount = 0;
            int sslCount = 0;

            foreach (string line in input)
            {
                Ipv7? ipv7 = new Ipv7(line);

                if (ipv7.SupportTLS)
                {
                    tlsCount++;
                }

                if (ipv7.SupportSSL)
                {
                    sslCount++;
                }
            }

            Console.WriteLine($"Part 1) How many IPs in your puzzle input support TLS? {tlsCount}");
            Console.WriteLine($"Part 2) How many IPs in your puzzle input support SSL? {sslCount}\n");
        }
    }
}
