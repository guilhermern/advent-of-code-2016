﻿namespace AdventOfCode.Day7
{
    public sealed class ABBADetector
    {
        public static bool DetectAbba(string value)
        {
            if (value.Length < 4)
            {
                return false;
            }

            for (int i = 0, j = 3; j < value.Length; i++, j++)
            {
                if (value[i] == value[j] &&
                    value[i + 1] == value[j - 1] &&
                    value[i] != value[i + 1])
                {
                    return true;
                }
            }

            return false;
        }
    }
}
