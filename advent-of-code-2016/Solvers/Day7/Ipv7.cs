﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Day7
{
    public sealed class Ipv7
    {
        public bool SupportTLS => _supportTLS;
        public bool SupportSSL => _supportSSL;

        private readonly List<string> _externalStrings = new List<string>();
        private readonly List<string> _internalStrings = new List<string>();

        private readonly bool _supportTLS;
        private readonly bool _supportSSL;

        public Ipv7(string value)
        {
            SeparateValue(value);

            _supportTLS = CheckTLSSupport();
            _supportSSL = CheckSSLSupport();
        }

        private void SeparateValue(string value)
        {
            string[] split = value.Split('[', ']');

            for (int i = 0; i < split.Length; i++)
            {
                if (i % 2 == 0)
                {
                    _externalStrings.Add(split[i]);
                }
                else
                {
                    _internalStrings.Add(split[i]);
                }
            }

        }

        private bool CheckTLSSupport()
        {
            bool hasExternalABBA = _externalStrings.Any(ABBADetector.DetectAbba);
            bool hasInternalABBA = _internalStrings.Any(ABBADetector.DetectAbba);

            return hasExternalABBA && !hasInternalABBA;
        }

        private bool CheckSSLSupport()
        {
            return ABADetector.DetectAbaCorrespondent(_externalStrings, _internalStrings);
        }
    }
}
