﻿using System.Collections.Generic;
using System.Text;

namespace AdventOfCode.Day7
{
    public static class ABADetector
    {
        public static bool DetectAbaCorrespondent(List<string> externals, List<string> internals)
        {
            foreach (string externalWord in externals)
            {
                if (externalWord.Length < 3)
                {
                    return false;
                }

                for (int i = 0, j = 2; j < externalWord.Length; i++, j++)
                {
                    if (externalWord[i] != externalWord[j] || externalWord[i] == externalWord[i + 1])
                    {
                        continue;
                    }

                    string? aba = new StringBuilder("aba") {[0] = externalWord[i], [1] = externalWord[i + 1], [2] = externalWord[j]}.ToString();

                    foreach (string internalWord in internals)
                    {
                        for (int k = 0, l = 2; l < internalWord.Length; k++, l++)
                        {
                            string? targetBab = new StringBuilder("bab"){[0] = aba[1], [1] = aba[0], [2] = aba[1]}.ToString();
                            string? currentBab = new StringBuilder("bab"){[0] = internalWord[k], [1] = internalWord[k + 1], [2] = internalWord[l]}.ToString();

                            if (targetBab == currentBab)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
    }
}
