﻿using System.Collections.Generic;

namespace AdventOfCode.Day6
{
    public sealed class CharCount
    {
        private readonly List<KeyValuePair<char, int>> _chars;

        public CharCount()
        {
            _chars = new List<KeyValuePair<char, int>>();
        }

        public void Add(char value)
        {
            if (TryGetValue(value, out int index))
            {
                KeyValuePair<char, int> newPair = new KeyValuePair<char, int>(value, _chars[index].Value + 1);
                _chars[index] = newPair;
            }
            else
            {
                _chars.Add(new KeyValuePair<char, int>(value, 0));
            }
        }

        public char GetMostUsedChar()
        {
            _chars.Sort((pair, valuePair) => valuePair.Value.CompareTo(pair.Value));

            return _chars[0].Key;
        }

        public char GetLeastUsedChar()
        {
            _chars.Sort((pair, valuePair) => valuePair.Value.CompareTo(pair.Value));

            return _chars[^1].Key;
        }

        private bool TryGetValue(char value, out int index)
        {
            for (int i = 0; i < _chars.Count; i++)
            {
                if (value != _chars[i].Key)
                {
                    continue;
                }

                index = i;
                return true;
            }

            index = -1;
            return false;
        }
    }
}
