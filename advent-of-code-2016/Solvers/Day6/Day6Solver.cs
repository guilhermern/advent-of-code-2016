﻿using System;
using System.Collections.Generic;

namespace AdventOfCode.Day6
{
    public sealed class Day6Solver : IDaySolver
    {
        private const string NAME = "Day6";

        public string Name => NAME;

        private readonly List<CharCount> _counters = new List<CharCount>();
        private int _numberOfColumns;

        private enum CharCountMethod
        {
            MostUsed,
            LeastUsed
        }

        public void Solve(string[] input)
        {
            Console.WriteLine("Day 6");

            _numberOfColumns = input[0].Length;

            string mostLikelyWord = GetCharPerColumn(input, CharCountMethod.MostUsed);
            string leastLikelyWord = GetCharPerColumn(input, CharCountMethod.LeastUsed);

            Console.WriteLine($"Part 1) What is the error-corrected version of the message being sent? {mostLikelyWord}");
            Console.WriteLine($"Part 2) What is the original message that Santa is trying to send? {leastLikelyWord}");
        }

        private string GetCharPerColumn(IEnumerable<string> input, CharCountMethod method)
        {
            for (int i = 0; i < _numberOfColumns; i++)
            {
                _counters.Add(new CharCount());
            }

            foreach (string line in input)
            {
                for (int i = 0; i < _numberOfColumns; i++)
                {
                    char c = line[i];
                    _counters[i].Add(c);
                }
            }

            string word = string.Empty;

            for (int i = 0; i < _numberOfColumns; i++)
            {
                word += method switch
                {
                    CharCountMethod.MostUsed => _counters[i].GetMostUsedChar(),
                    CharCountMethod.LeastUsed => _counters[i].GetLeastUsedChar(),

                    _ => throw new ArgumentOutOfRangeException(nameof(method), method, null)
                };
            }

            _counters.Clear();
            return word;
        }
    }
}
