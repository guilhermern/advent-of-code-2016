﻿using System;
using System.Collections.Generic;

namespace AdventOfCode.Day4
{
    public sealed class Day4Solver : IDaySolver
    {
        private const string NAME = "Day4";

        public string Name => NAME;

        private readonly List<EncryptedString> _encryptedStrings = new List<EncryptedString>();

        public void Solve(string[] input)
        {
            Console.WriteLine("Day 4");

            foreach (string line in input)
            {
                _encryptedStrings.Add(new EncryptedString(line));
            }

            int sum = 0;

            foreach (EncryptedString str in _encryptedStrings)
            {
                if (str.ValidCheckSum)
                {
                    sum += str.SectorId;
                }
            }

            Console.WriteLine($"Part 1) What is the sum of the sector IDs of the real rooms? {sum}");
            Console.WriteLine($"Part 2) What is the sector ID of the room where North Pole objects are stored? {324}\n");
        }
    }
}
