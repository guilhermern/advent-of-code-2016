﻿namespace AdventOfCode.Day4
{
    public static class ShiftCipherDecrypt
    {
        private const string PLAIN = "abcdefghijklmnopqrstuvwxyz";
        private const int ASCII_DIFFERENCE = 97;

        public static string Decrypt(string sentence, int shift)
        {
            string? decryptedName = string.Empty;

            sentence = sentence.ToLower();

            foreach (char c in sentence)
            {
                if (c == '-')
                {
                    decryptedName += " ";
                }
                else
                {
                    int x = (c - ASCII_DIFFERENCE);

                    int newValue = (x + shift) % PLAIN.Length;

                    decryptedName += PLAIN[newValue];
                }
            }

            return decryptedName;
        }
    }
}
