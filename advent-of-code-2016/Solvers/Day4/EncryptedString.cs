﻿using System.Collections.Generic;

namespace AdventOfCode.Day4
{
    public sealed class EncryptedString
    {
        public string DecryptedName => _decryptedName;
        public bool ValidCheckSum => _validCheckSum;
        public int SectorId => _sectorId;

        private readonly List<KeyValuePair<char, int>> _characters = new List<KeyValuePair<char, int>>();

        private string _decryptedName = string.Empty;
        private string _checkSum = string.Empty;
        private int _sectorId;
        private bool _validCheckSum;

        public EncryptedString(string input)
        {
            ProcessInput(input);
            Sort();
            ValidateCheckSum();
        }

        private void ProcessInput(string input)
        {
            string[] split = input.Split('-');
            string lastPos = split[^1][..(split[^1].Length - 1)];
            string[] lastPosSplit = lastPos.Split('[');

            _sectorId = int.Parse(lastPosSplit[0]);
            _checkSum = lastPosSplit[1];

            for (int i = 0; i < split.Length - 1; i++)
            {
                string subString = split[i];

                foreach (char key in subString)
                {
                    if (TryGetValue(key, out int index))
                    {
                        KeyValuePair<char, int> newPair = new KeyValuePair<char, int>(key, _characters[index].Value + 1);
                        _characters[index] = newPair;
                    }
                    else
                    {
                        _characters.Add(new KeyValuePair<char, int> (key, 0));
                    }
                }
            }

            string encName = input[..^1].Split('[')[0];

            int sepIndex = -1;
            for (int i = encName.Length - 1; i >= 0; i--)
            {
                if (encName[i] != '-')
                {
                    continue;
                }

                sepIndex = i;
                break;
            }

            _decryptedName = ShiftCipherDecrypt.Decrypt(encName.Remove(sepIndex).ToLower(), _sectorId);
        }

        private bool TryGetValue(char key, out int index)
        {
            for (int i = 0; i < _characters.Count; i++)
            {
                if (key != _characters[i].Key)
                {
                    continue;
                }

                index = i;
                return true;
            }

            index = -1;
            return false;
        }

        private void Sort()
        {
            _characters.Sort((pair, valuePair) => pair.Value != valuePair.Value ? valuePair.Value.CompareTo(pair.Value) : pair.Key.CompareTo(valuePair.Key));
        }

        private void ValidateCheckSum()
        {
            int checkSumSize = _checkSum.Length;
            string? listCheckSum = string.Empty;

            if (_characters.Count < checkSumSize)
            {
                _validCheckSum = false;
                return;
            }

            for (int i = 0; i < checkSumSize; i++)
            {
                listCheckSum += _characters[i].Key;
            }

            _validCheckSum = listCheckSum == _checkSum;
        }
    }
}
