﻿using System;
using System.Text.RegularExpressions;

namespace AdventOfCode.Day3
{
    public sealed class Day3Solver : IDaySolver
    {
        private const string NAME = "Day3";

        public string Name => NAME;

        public void Solve(string[] input)
        {
            Console.WriteLine("Day 3");

            int validHorizontalTriangles = CountHorizontalValidTriangles(input);

            Console.WriteLine($"Part 1) how many of the listed triangles are possible? {validHorizontalTriangles}");

            int validVerticalTriangles = CountVerticalValidTriangles(input);

            Console.WriteLine($"Part 2) In your puzzle input, and instead reading by columns, how many of the listed triangles are possible? {validVerticalTriangles}\n");
        }

        private int CountHorizontalValidTriangles(string[] input)
        {
            int validTriangles = 0;

            foreach (string line in input)
            {
                string[] split = Regex.Replace(line, @"\s+", " ")[1..].Split(" ");

                int sideA = int.Parse(split[0]);
                int sideB = int.Parse(split[1]);
                int sideC = int.Parse(split[2]);

                if (IsTriangleValid(sideA, sideB, sideC))
                {
                    validTriangles++;
                }
            }

            return validTriangles;
        }

        private int CountVerticalValidTriangles(string[] input)
        {
            int validTriangles = 0;

            for (int i = 0; i < input.Length; i++)
            {
                if ((i + 1) % 3 != 0)
                {
                    continue;
                }

                string[] line1 = Regex.Replace(input[i - 2], @"\s+", " ")[1..].Split(" ");
                string[] line2 = Regex.Replace(input[i - 1], @"\s+", " ")[1..].Split(" ");
                string[] line3 = Regex.Replace(input[i], @"\s+", " ")[1..].Split(" ");

                int sideA1 = int.Parse(line1[0]);
                int sideB1 = int.Parse(line2[0]);
                int sideC1 = int.Parse(line3[0]);

                int sideA2 = int.Parse(line1[1]);
                int sideB2 = int.Parse(line2[1]);
                int sideC2 = int.Parse(line3[1]);

                int sideA3 = int.Parse(line1[2]);
                int sideB3 = int.Parse(line2[2]);
                int sideC3 = int.Parse(line3[2]);


                if (IsTriangleValid(sideA1, sideB1, sideC1))
                {
                    validTriangles++;
                }

                if (IsTriangleValid(sideA2, sideB2, sideC2))
                {
                    validTriangles++;
                }

                if (IsTriangleValid(sideA3, sideB3, sideC3))
                {
                    validTriangles++;
                }
            }

            return validTriangles;
        }

        private bool IsTriangleValid(int sideA, int sideB, int sideC)
        {
            return (sideA + sideB > sideC) &&
                   (sideA + sideC > sideB) &&
                   (sideB + sideC > sideA);
        }
    }
}
