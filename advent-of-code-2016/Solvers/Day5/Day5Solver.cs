﻿using System;
using System.Text;

namespace AdventOfCode.Day5
{
    public sealed class Day5Solver : IDaySolver
    {
        private const string NAME = "Day5";

        public string Name => NAME;

        private enum PasswordGenerateMethod
        {
            Sequential,
            PositionDetermined
        }

        public void Solve(string[] input)
        {
            Console.WriteLine("Day 5");

            string doorId = input[0];

            string sequentialPassword = GeneratePassword(doorId, PasswordGenerateMethod.Sequential);
            string posDeterminedPassword = GeneratePassword(doorId, PasswordGenerateMethod.PositionDetermined);

            Console.WriteLine($"Given the actual Door ID, what is the password? {sequentialPassword}");
            Console.WriteLine($"Given the actual Door ID and this new method, what is the password? {posDeterminedPassword}\n");
        }

        private static string GeneratePassword(string doorId, PasswordGenerateMethod method)
        {
            Console.WriteLine("Generating password");

            StringBuilder? password = new StringBuilder("________");
            int charsFound = 0;

            int index = 0;

            while (charsFound < 8)
            {
                string hash = Md5Generator.Generate(doorId + index);

                if (hash[..5] != "00000")
                {
                    index++;
                    continue;
                }

                switch (method)
                {
                    case PasswordGenerateMethod.Sequential:
                    {
                        password[charsFound] = hash[5];
                        break;
                    }

                    case PasswordGenerateMethod.PositionDetermined:
                    {
                        bool parsable = int.TryParse(hash[5].ToString(), out int charPos);
                        bool outOfRange = charPos >= password.Length;
                        bool spotOccupied = outOfRange || password[charPos] != '_';

                        if (!parsable || outOfRange || spotOccupied)
                        {
                            index++;
                            continue;
                        }

                        password[charPos] = hash[6];
                        break;
                    }
                    default:
                    {
                        throw new ArgumentOutOfRangeException(nameof(method), method, null);
                    }
                }

                Console.WriteLine(password.ToString());

                charsFound++;
                index++;
            }

            Console.WriteLine("Password generated\n");

            return password.ToString();
        }
    }
}
