﻿using System.Security.Cryptography;
using System.Text;

namespace AdventOfCode.Day5
{
    public static class Md5Generator
    {
        public static string Generate(string input)
        {
            using MD5? md5 = MD5.Create();

            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder? sb = new StringBuilder();

            foreach (byte b in hash)
            {
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }
    }
}
