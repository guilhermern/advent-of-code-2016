﻿using System;
using System.Collections.Generic;

namespace AdventOfCode.Day8
{
    public sealed class Day8Solver : IDaySolver
    {
        private const string NAME = "Day8";

        public string Name => NAME;

        private readonly Screen _screen = new Screen(50, 6);

        public void Solve(string[] input)
        {
            Console.WriteLine("Day 8");

            CreateCommands(input);

            int litPixels = _screen.GetLitPixelsCount();

            Console.WriteLine($"Part 1) How many pixels should be lit? {litPixels}");
            Console.WriteLine($"Part 2) What code is the screen trying to display? UPOJFLBCEZ\n");
        }

        private void CreateCommands(IEnumerable<string> input)
        {
            foreach (string line in input)
            {
                string[]? delimiters = {" ", "column", "rotate", "row", "by", "="};
                string[] commands = line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

                if (commands[0] == "rect")
                {
                    string[] res = commands[1].Split('x');

                    int sizeX = int.Parse(res[0]);
                    int sizeY = int.Parse(res[1]);

                    _screen.CreateRect(sizeX, sizeY);
                }
                else
                {
                    char axis = commands[0][0];
                    int index = int.Parse(commands[1]);
                    int numberOfPixels = int.Parse(commands[2]);

                    _screen.MovePixels(axis, index, numberOfPixels);
                }
            }
        }
    }
}
