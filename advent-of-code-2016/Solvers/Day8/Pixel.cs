﻿namespace AdventOfCode.Day8
{
    public class Pixel
    {
        public int X;
        public int Y;
        public bool Lit;

        public Pixel(int x, int y, bool lit)
        {
            X = x;
            Y = y;
            Lit = lit;
        }
    }
}
