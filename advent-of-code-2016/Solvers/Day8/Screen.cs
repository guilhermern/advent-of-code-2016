﻿using System;
using System.Collections.Generic;

namespace AdventOfCode.Day8
{
    public sealed class Screen
    {
        private readonly int _width;
        private readonly int _height;

        private char[,]? _grid;
        private readonly List<Pixel> _pixels = new List<Pixel>();

        public Screen(int width, int height)
        {
            _width = width;
            _height = height;

            Initialize();
        }

        public void MovePixels(char axis, int index, int numberOfPixels)
        {
            List<Pixel> targetPixels = axis == 'x' ? _pixels.FindAll(pixel => pixel.X == index) : _pixels.FindAll(pixel => pixel.Y == index);

            ChangePixelValue(targetPixels, false);

            if (axis == 'x')
            {
                targetPixels.ForEach(pixel =>
                {
                    pixel.Y = (pixel.Y + numberOfPixels) % _height;
                });
            }
            else
            {
                targetPixels.ForEach(pixel =>
                {
                    pixel.X = (pixel.X + numberOfPixels) % _width;
                });
            }

            ChangePixelValue(targetPixels, true);
        }

        public void CreateRect(int width, int height)
        {
            if (width > _width || height > _height)
            {
                return;
            }

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (_grid[j, i] != '.')
                    {
                        continue;
                    }

                    _grid[j, i] = '#';
                    _pixels.Add(new Pixel(j, i, true));
                }
            }
        }

        public void Show()
        {
            Console.WriteLine();

            for (int i = 0; i < _height; i++)
            {
                for (int j = 0; j < _width; j++)
                {
                    Console.Write(_grid[j, i]);
                }

                Console.WriteLine();
            }
        }

        public int GetLitPixelsCount()
        {
            return _pixels.FindAll(pixel => pixel.Lit).Count;
        }

        private void Initialize()
        {
            _grid = new char[_width, _height];

            for (int i = 0; i < _height; i++)
            {
                for (int j = 0; j < _width; j++)
                {
                    _grid[j, i] = '.';
                }
            }
        }

        private void ChangePixelValue(List<Pixel> pixels, bool lit)
        {
            pixels.ForEach(pixel =>
            {
                if (lit)
                {
                    _grid[pixel.X, pixel.Y] = '#';
                }
                else
                {
                    _grid[pixel.X, pixel.Y] = '.';
                }

                pixel.Lit = lit;
            });
        }
    }
}
