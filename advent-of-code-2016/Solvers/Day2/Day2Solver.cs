﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace AdventOfCode.Day2
{
    public sealed class Day2Solver : IDaySolver
    {
        private const string NAME = "Day2";

        public string Name => NAME;

        private static readonly char[][] _grid1 =
        {
            new[] {'1', '2', '3'},
            new[] {'4', '5', '6'},
            new[] {'7', '8', '9'},
        };

        private static readonly char[][] _grid2 =
        {
            new[] {'0', '0', '1', '0', '0'},
            new[] {'0', '2', '3', '4', '0'},
            new[] {'5', '6', '7', '8', '9'},
            new[] {'0', 'A', 'B', 'C', '0'},
            new[] {'0', '0', 'D', '0', '0'},
        };

        private Point _currentPos = new Point(1, 1);
        private int _gridWidth = 3;
        private int _gridHeight = 3;
        private char[][] _currentGrid = _grid1;

        public void Solve(string[] input)
        {
            Console.WriteLine("Day 2");

            string code = GenerateCode(input);

            Console.WriteLine($"Part 1) What is the bathroom code ? {code}");

            _gridWidth = 5;
            _gridHeight = 5;
            _currentGrid = _grid2;
            _currentPos = new Point(0, 2);

            code = GenerateCode(input);

            Console.WriteLine($"Part 2) What is the bathroom code ? {code}\n");
        }

        private string GenerateCode(IEnumerable<string> input)
        {
            string code = string.Empty;

            foreach (string line in input)
            {
                foreach (char dir in line)
                {
                    Move(dir);
                }

                string digit = _currentGrid[_currentPos.Y][_currentPos.X].ToString();

                code += digit;
            }

            return code;
        }

        private void Move(in char c)
        {
            bool validMove = false;
            Point targetPos = new Point();

            switch (c)
            {
                case 'U':
                {
                    targetPos = new Point(_currentPos.X, _currentPos.Y - 1);
                    validMove = IsPositionValid(targetPos);
                    break;
                }

                case 'L':
                {
                    targetPos = new Point(_currentPos.X - 1, _currentPos.Y);
                    validMove = IsPositionValid(targetPos);
                    break;
                }

                case 'R':
                {
                    targetPos = new Point(_currentPos.X + 1, _currentPos.Y);
                    validMove = IsPositionValid(targetPos);
                    break;
                }

                case 'D':
                {
                    targetPos = new Point(_currentPos.X, _currentPos.Y + 1);
                    validMove = IsPositionValid(targetPos);
                    break;
                }
            }

            if (validMove)
            {
                _currentPos = targetPos;
            }
        }

        private bool IsPositionValid(Point target)
        {
            if ((target.X < 0 || target.X >= _gridWidth) || (target.Y < 0 || target.Y >= _gridHeight))
            {
                return false;
            }

            return _currentGrid[target.Y][target.X] != '0';
        }
    }
}
