﻿using System.Text;

namespace AdventOfCode.Day9
{
    public static class StringDecompressor
    {
        private const char COMMAND_SEPARATOR_CHAR = 'x';
        private const char COMMAND_START_CHAR = '(';
        private const char COMMAND_END_CHAR = ')';

        public static string Decompress(string input)
        {
            StringBuilder decompressStrBuilder = new StringBuilder();

            for (int currentIndex = 0; currentIndex < input.Length; currentIndex++)
            {
                if (!TryFindCommand(input, currentIndex, out int endIndex, out int range, out int repeatCount))
                {
                    decompressStrBuilder.Append(input[currentIndex]);
                    continue;
                }

                string resultString = ExecuteCommand(input, endIndex + 1, range, repeatCount);
                decompressStrBuilder.Append(resultString);
                currentIndex = endIndex + range;
            }

            return decompressStrBuilder.ToString();
        }

        private static string ExecuteCommand(string input, int startIndex, int range, int repeatCount)
        {
            StringBuilder resultBuilder = new StringBuilder();
            StringBuilder subStrBuilder = new StringBuilder();

            for (int i = startIndex; i < startIndex + range; i++)
            {
                subStrBuilder.Append(input[i]);
            }

            string subStr = subStrBuilder.ToString();

            for (int i = 1; i <= repeatCount; i++)
            {
                resultBuilder.Append(subStr);
            }

            return resultBuilder.ToString();
        }

        private static bool TryFindCommand(string input, int currentIndex, out int endIndex, out int range, out int repeatCount)
        {
            char character = input[currentIndex];

            range = 0;
            repeatCount = 0;
            endIndex = 0;

            if (character != COMMAND_START_CHAR)
            {
                return false;
            }

            for (int i = currentIndex + 1; i < input.Length; i++)
            {
                if (input[i] != COMMAND_END_CHAR)
                {
                    continue;
                }

                endIndex = i;
                break;
            }

            int length = (endIndex - 1) - currentIndex;

            string pair = input.Substring(currentIndex + 1, length);

            string[] split = pair.Split(COMMAND_SEPARATOR_CHAR);

            range = int.Parse(split[0]);
            repeatCount = int.Parse(split[1]);

            return true;
        }
    }
}
