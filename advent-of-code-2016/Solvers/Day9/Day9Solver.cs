﻿using System;

namespace AdventOfCode.Day9
{
    public sealed class Day9Solver : IDaySolver
    {
        private const string NAME = "Day9";

        public string Name => NAME;

        public void Solve(string[] input)
        {
            Console.WriteLine("Day 9");

            string decompressed = StringDecompressor.Decompress(input[0]);

            Console.WriteLine($"Part 1) What is the decompressed length of the file (your puzzle input) ? Don't count whitespace. {decompressed.Length}\n");
        }
    }
}
