﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace AdventOfCode.Day1
{
    public sealed class Day1Solver : IDaySolver
    {
        private const string NAME = "Day1";

        private const char RIGHT_DIR = 'R';
        private const char LEFT_DIR = 'L';

        public string Name => NAME;

        private static readonly List<Vector2> _directions = new List<Vector2>()
        {
            new Vector2(0, 1), // north
            new Vector2(1, 0), //east
            new Vector2(0, -1), //south
            new Vector2(-1, 0) //west
        };

        private static readonly Vector2 _origin = new Vector2(0, 0);

        private Vector2 _currentPos = new Vector2(0, 0);
        private Vector2 _intersectionPoint;

        private readonly List<Vector2> _path = new List<Vector2>();
        private int _currentDirId = 0;

        public void Solve(string[] input)
        {
            Console.WriteLine("Day 1");

            string[] commands = input[0].Split(new[] {", "}, StringSplitOptions.None);
            ProcessCommands(commands);

            int hqDistance = ManhattanDistance(_currentPos, _origin);
            int intersectionPointDistance = ManhattanDistance(_intersectionPoint, _origin);

            Console.WriteLine($"Part 1) How many blocks away is Easter Bunny HQ ? {hqDistance}");
            Console.WriteLine($"Part 2) How many blocks away is the first location you visit twice? ? {intersectionPointDistance}\n");
        }

        private void ProcessCommands(IEnumerable<string> commands)
        {
            foreach (string command in commands)
            {
                char targetDir = command[0];
                int blocks = int.Parse(command[1..]);

                TurnDirection(targetDir);

                while (blocks > 0)
                {
                    _currentPos += _directions[_currentDirId] * 1;
                    _path.Add(_currentPos);

                    blocks--;
                }
            }

            SearchIntersection();
        }

        private void SearchIntersection()
        {
            for (int i = 0; i < _path.Count - 1; i++)
            {
                for (int j = 0; j < _path.Count - 1; j++)
                {
                    if (i == j || _path[i] != _path[j])
                    {
                        continue;
                    }

                    _intersectionPoint = _path[i];
                    return;
                }
            }
        }

        private void TurnDirection(char targetDir)
        {
            switch (targetDir)
            {
                case RIGHT_DIR:
                {
                    bool increment = _currentDirId + 1 < _directions.Count;
                    _currentDirId = increment ? _currentDirId + 1 : _currentDirId = 0;

                    break;
                }

                case LEFT_DIR:
                {
                    bool decrement = _currentDirId - 1 >= 0;
                    _currentDirId = decrement ? _currentDirId - 1 : _directions.Count - 1;
                    break;
                }
            }
        }

        private int ManhattanDistance(Vector2 a, Vector2 b)
        {
            return (int)((Math.Abs(a.X - b.X)) + (Math.Abs(a.Y - b.Y)));
        }
    }
}
